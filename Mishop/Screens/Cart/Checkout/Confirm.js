import React, { useEffect, useState } from "react";
import { View, StyleSheet, Dimensions, ScrollView, Button } from "react-native";
import { Text, Left, Right, ListItem, Thumbnail, Body } from "native-base";
import Toast from "react-native-toast-message";

import * as actions from "../../../Redux/Actions/cartAction";
import axios from "axios";
import baseURL from "../../../assets/common/baseUrl";




//Redux
import { connect } from "react-redux";

let { width, height } = Dimensions.get("window");

const Confirm = (props) => {


    const finalOrder = props.route.params;

    // Add this
    const [productUpdate, setProductUpdate] = useState();
    useEffect(() => {
        if(finalOrder) {
          getProducts(finalOrder);
        }
      return () => {
        setProductUpdate();
      };
    }, [props]);

    const getProducts = (x) => {
        const order = x.order.order;
        var products = [];
        if(order) {
            order.orderItems.forEach((cart) => {
                axios
                  .get(`${baseURL}products/${cart.product}`)
                  .then((data) => {
                    products.push(data.data);
                    setProductUpdate(products);
                  })
                  .catch((e) => {
                    console.log(e);
                  });
              });
        }
        
      };
 
    const confirmOrder = () => {
        const order = finalOrder.order.order;
        axios
          .post(`${baseURL}orders`, order)
          .then((res) => {
            if (res.status == 200 || res.status == 201) {
              Toast.show({
                topOffset: 60,
                type: "success",
                text1: "Order Completed",
                text2: "",
              });
              setTimeout(() => {
                props.clearCart();
                props.navigation.navigate("Cart");
              }, 500);
            }
          })
          .catch((error) => {
            Toast.show({
              topOffset: 60,
              type: "error",
              text1: "Something went wrong",
              text2: "Please try again",
            });
          });
      };
 
    const confirm = props.route.params
 
    return (
        <ScrollView contentContainerStyle={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                Confirmer la commande
                </Text>
                {props.route.params ? (
          <View style={{ borderWidth: 1, borderColor: "orange" }}>
            <Text style={styles.title}>Shipping to:</Text>
            <View style={{ padding: 8 }}>
              <Text>Adresse: {finalOrder.order.order.shippingAddress1}</Text>
              <Text>Adresse2: {finalOrder.order.order.shippingAddress2}</Text>
              <Text>Ville: {finalOrder.order.order.city}</Text>
              <Text>Code Postal: {finalOrder.order.order.zip}</Text>
              <Text>Pays: {finalOrder.order.order.country}</Text>
            </View>
            <Text style={styles.title}>Produits:</Text>
            {/* CHANGE THIS */}
            {productUpdate && (
              <>
                {productUpdate.map((x) => {
                  return (
                    <ListItem style={styles.listItem} key={x.name} avatar>
                      <Left>
                        <Thumbnail source={{ uri: x.image }} />
                      </Left>
                      <Body style={styles.body}>
                        <Left>
                          <Text>{x.name}</Text>
                        </Left>
                        <Right>
                          <Text>{x.price}€</Text>
                        </Right>
                      </Body>
                    </ListItem>
                  );
                })}
              </>
            )}
          </View>
        ) : null}
                <View style={{ alignItems: 'center', margin: 20 }}>
                    <Button title={'Passer la commande'} onPress={confirmOrder} />
                </View>
            </View>
        </ScrollView>
    )
}
const mapDispatchToProps = (dispatch) => {
    return {
        clearCart: () => dispatch(actions.clearCart)
    }
}
 
const styles = StyleSheet.create({
    container: {
        height: height,
        padding: 8,
        alignContent: 'center',
        backgroundColor: 'white'
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 8
    },
    title: {
        alignSelf: 'center',
        margin: 8,
        fontSize: 16,
        fontWeight: 'bold'
    },
    listItem: {
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'center',
        width: width / 1.2
    },
    body: {
        margin: 5,
        alignItems: 'center',
        flexDirection: 'row'
    }
})
 
export default connect(null, mapDispatchToProps)(Confirm);


