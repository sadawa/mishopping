import React from "react"
import {View,StyleSheet, Dimensions,Image} from "react-native"
import {Content, Left , Body ,Thumbnail , Text, ListItem} from 'native-base'

let {width} = Dimensions.get("window")

const SearchedProduct = (props) => {

        const {productsFiltered} = props
        
     return(
         <Content style={{width: width}}>
             {productsFiltered.length > 0 ? (
                 productsFiltered.map((item) => (
                     <ListItem 
                     onPress={() => props.navigation.navigate("Produit",{item: item})}
                     key={item.id}
                     avatar>
                         <Left>
                             <Thumbnail
                                source={{uri: item.image ? item.image: 'https://cdn.pixabay.com/photo/2012/04/01/17/29/box-23649_960_720.png'}}
                             />
                             {/* <Image style={{flex: 1, width:width,height:35}} source={{uri: item.image}} /> */}
                         </Left>
                         <Body>
                             <Text>{item.name}</Text>
                             <Text note>{item.description}</Text>
                         </Body>
                     </ListItem>
                 ))
             ) : (
                 <View style={StyleSheet.center}>
                     <Text style={{alignSelf: 'center'}}>
                        On trouve pas le produit essaie encore 
                     </Text>
                 </View>
             )}
         </Content>
     )
}

const styles = StyleSheet.create({
        center:{
            justifyContent: "center",
            alignItems: "center"
        }
})

export default SearchedProduct