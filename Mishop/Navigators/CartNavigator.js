import React from 'react'
import {createStackNavigator} from "@react-navigation/stack"

import Cart from '../Screens/Cart/Cart'
import CheckoutNavigator from './CheckoutNavigator'

const Stack = createStackNavigator()

function MyStack() {

    return(
    <Stack.Navigator>
        <Stack.Screen 
            name='Panier' 
            component={Cart} 
            options={{
                headerShown: false,
            }}      
        />
        <Stack.Screen 
            name='Payer' 
            component={CheckoutNavigator}
            options={{
                headerShown: false,
            }}      
        />
    
    </Stack.Navigator>
    )
}

export default function CartNavigator(){
    return <MyStack/>
}