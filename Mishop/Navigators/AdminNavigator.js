import React from 'react'
import {createStackNavigator} from "@react-navigation/stack"

import Order from '../Screens/Admin/Order'
import Categories from '../Screens/Admin/Categories'
import Products from '../Screens/Admin/Products'
import ProductForm from '../Screens/Admin/ProductForm'


const Stack = createStackNavigator()

function MyStack() {

    return(
    <Stack.Navigator>
        <Stack.Screen 
            name='Products' 
            component={Products} 
            options={{
                title: "Produits",
            }}      
        />
        <Stack.Screen 
            name='Categories' 
            component={Categories}
            
        />
        <Stack.Screen 
            name='Orders' 
            component={Order}
            options={{
                title: "Livraison",
            }}      
         
        />
        <Stack.Screen 
            name='ProductsForm' 
            component={ProductForm}
            options={{
                title: "NouveauProduits",
            }}  
        />
    
    </Stack.Navigator>
    )
}

export default function AdminNavigator(){
    return <MyStack/>
}