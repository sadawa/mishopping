import React, {useEffect, useState} from 'react'
import {Image,StyleSheet,Dimensions,View,ScrollView} from 'react-native'
import Swiper from 'react-native-swiper'

let {width} = Dimensions.get("window")


const Banner = () => {
    const [bannerData,setBannerData] = useState([])

    useEffect(() => {
        setBannerData([
            "https://image.freepik.com/psd-gratuit/modele-banniere-medias-sociaux-publication-instagram-pour-mode-streetwear_335470-14.jpg",
            "https://cdn.dribbble.com/users/3089115/screenshots/6930601/dribbble_post_2_4x.jpg?compress=1&resize=400x300",
            "https://cdn2.vectorstock.com/i/1000x1000/68/01/holy-spirit-streetwear-simple-vintage-vector-38386801.jpg"]
            )
        return () => {
            setBannerData([])
        }
    }, [])

    return(
        <ScrollView>
                    <View style={styles.container}>
            <View style={styles.swiper}>
                <Swiper
                 style={{ height: width / 2 }}
                 showButtons={false}
                 autoplay={true}
                 autoplayTimeout={2}
                >
                    {bannerData.map((item) => {
                        return(
                            <Image
                                key={item}
                                style={styles.imageBanner}
                                resizeMode='contain'
                                source={{uri: item}}/>

                        )
                    })}
                </Swiper>
                <View style={{height: 20}} ></View>
            </View>
        </View>
    
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1 ,
        backgroundColor: 'gainsboro'
    },
    swiper:{
        width: width,
        alignItems: 'center',
        marginTop: 10
    },
    imageBanner:{
        height: width / 2 ,
        width: width - 40 , 
        borderRadius: 10, 
        marginHorizontal: 20
    }
})

export default Banner