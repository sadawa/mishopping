import {ADD_TO_CART,REMOVE_TO_CART,CLEAR_CART} from "../Reducers/constants"



export const addToCart = (payload) => {

    return{
        type: ADD_TO_CART,
        payload
    }
}

export const removeFromCart = (payload) => {
    return {
        type: REMOVE_TO_CART,
        payload
    }
}

export const clearCart = () => {
    return{
        type: CLEAR_CART
    }
}