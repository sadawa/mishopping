import express from "express"
import {getOrder,addOrder, getAllOrdersPopulate,getOrderPopulateId,updateOrderById,deleteOrder,totalSales,getAllOrdersCount,getUserOrders} from '../controllers/orderController.js'

const router = express.Router()

router.route('/').get(getOrder).post(addOrder)
router.route('/populate').get(getAllOrdersPopulate)
router.route('/:id').get(getOrderPopulateId).put(updateOrderById).delete(deleteOrder)
router.route('/get/totalsales').get(totalSales)
router.route('/get/count/order').get(getAllOrdersCount)
router.route('/get/userorders/:userid').get(getUserOrders)



export default router