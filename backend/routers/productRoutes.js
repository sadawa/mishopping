import express from "express"
import multer from "multer"
let multParse = multer()
const router = express.Router()
import {addProduct,getAllProducts,getProduct,getAllProductsPopulate,updateProduct,deleteProduct,getAllProductsCount,getProductsFeatured,getProductsFeaturedCount,galleryImageProduct,upload} from "../controllers/productController.js"



//If you want use the function delete getallProducts and chage to getAllProductsSelect or create new route for function 
// import getAllProductsSelect  from "../controllers/productController.js"


router.route('/').get(getAllProducts).post(upload.single('image'),addProduct)
router.route('/star').get(getAllProductsPopulate)
router.route('/get/count').get(getAllProductsCount)
router.route('/get/featured').get(getProductsFeatured)
router.route('/get/featured/:count').get(getProductsFeaturedCount)
router.route('/:id').get(getProduct).put(upload.single('image'),updateProduct).delete(deleteProduct)
router.route('/gallery-images/:id').put(upload.array('images',8),galleryImageProduct)


export default router