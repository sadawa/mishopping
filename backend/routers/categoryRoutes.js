import express from "express"
const router = express.Router()


import { addCategory,deleteCategory,getAllCategory,getCategoryById,updateCategoryById } from "../controllers/categoryController.js"


router.route('/').post(addCategory).get(getAllCategory)
router.route('/:id').delete(deleteCategory).get(getCategoryById).put(updateCategoryById)


export default router