import express from "express"
const router = express.Router()

import {addUser,getAllUser,getUserById,loginUser,deleteUser,updateUserAdmin,updateUser,getAllUsersCount} from "../controllers/userController.js"

router.route('/').get(getAllUser)
router.route('/get/count').get(getAllUsersCount)
router.route('/register').post(addUser)
router.route('/:id').get(getUserById).put(updateUser).delete(deleteUser)
router.route('/login').post(loginUser)
router.route('/admin/:id').put(updateUserAdmin)

export default router