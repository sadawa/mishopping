import mongoose from "mongoose"

const reviewSchema = mongoose.Schema({
    name:{type: String, required: true},
    comment:{type: String, required: true},
    user:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User"
    },
},{
    timestamps: true
})

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description:{
        type:String,
        required: true
    },
    richDescription:{
        type:String,
        default:''
    },
    image: {
        type:String,
        default: ''
    },
    images:[{
        type: String
    }],
    brand:{
        type: String,
        default: ''
    },
    price:{
        type:Number,
        default: 0
    },
    category:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Category',
        required:true
    },
    countInStock: {
        type:Number,
        required: true,
        min: 0,
        max: 100000,
    },
    rating: {
        type:Number,
        default : 0
    },
    numReviews:{
        type: Number,
        default: 0
    },
    isFeatured: {
        type: Boolean,
        default: false
    },
    dateCreated: {
        type:Date,
        default: Date.now
    }


})
// Create 2 id for frontend 
productSchema.virtual('id').get(function () {
    return this._id.toHexString();
})

productSchema.set('toJSON', {
    virtuals:true
})

const Product = mongoose.model("Product",productSchema)

export default Product