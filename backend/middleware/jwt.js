import expressJwt from "express-jwt"


function authJwt() {
    const secret = process.env.JWT_SECRET 
    return expressJwt({
        secret,
        algorithms: ['HS256'],
        isRevoked: isRevoked
    }).unless({
        path: [
            // Use regular expression for url 
            {url: /\/public\/uploads(.*)/ , methods: ['GET', 'OPTIONS'] },
            {url: /\/api\/produits(.*)/  , method:['GET','OPTIONS']},
            {url: /\/api\/users(.*)/ , methods :['GET','OPTIONS'] },
            // {url: /\/api\/users(.*)/ , methods :['GET'] },
            {url: /\/api\/categories(.*)/  , method:['GET','OPTIONS']},
            {url: /\/api\/orders(.*)/,methods: ['GET', 'OPTIONS', 'POST']},
            '/api/users/login',
            '/api/users/register'
        ]
    })
}

async  function isRevoked(req,payload,done){
        if(!payload.isAdmin || payload.isModerator ){
            done(null,true)
        }

        done()
}

export default authJwt