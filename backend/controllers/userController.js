import User from "../models/userModels.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken"

// add new User (register)
const addUser = async(req,res) => {
    let user = new User({
        name: req.body.name,
        email:req.body.email,
        passwordHash: bcrypt.hashSync(req.body.password, 15),
        phone: req.body.phone,
        isAdmin: req.body.isAdmin,
        isModerator: req.body.isModerator,
        apartment: req.body.apartment,
        zip: req.body.zip,
        city: req.body.city,
        country: req.body.country,
        street: req.body.street
    })
    user = await user.save()

    if(!user){
    return res.status(404).send('User is not created')
    } else{
        res.send(user)
    }
}

//Get all Users excluing password
const getAllUser = async (req,res) => {
    const userList = await User.find().select('-passwordHash');

    if(!userList) {
        res.status(500).json({success: false})
    } else{
        res.status(200).send(userList)
    }
}

// Get one User by Id 
const getUserById = async(req,res) => {
    const user = await User.findById(req.params.id).select('-passwordHash')
    if(!user){
        return res.status(404).send('User is not selected')
        } else{
            res.send(user)
        }
}
//Login User
const loginUser = async(req,res) => {
    const user = await User.findOne({ email: req.body.email})
    

    if(!user){
        return res.status(400).send('The User not found')
    }

    // console.log("pass1",req.body.password)
    // console.log("pass2",user.passwordHash)
if(user && bcrypt.compareSync(req.body.password, user.passwordHash)){
    
    const token = jwt.sign(
        {
            userId: user.id,
            isAdmin: user.isAdmin
        },
        process.env.JWT_SECRET,
        {expiresIn: '360d'}
    )
  
    res.status(200).send({user: user.email, token:token})
   
} else {
   res.status(400).send("Password or email is wrong please try again")
}

 
}

// delete User
const deleteUser = async(req,res) => {
    User.findByIdAndRemove(req.params.id).then(user => {
        if(user){
            return res.status(200).json({success: true, message: "User is delete"})
        } else {
            return res.status(401).json({success: false, message: "User is not delete"})
        }
    }).catch(err =>{
        return res.status(400).json({success: false, error: err})
    })
}

//Get all Users Count
const getAllUsersCount = async (req,res) => {
    const userCount = await User.countDocuments()


    if(!userCount){
        return res.status(500).json({sucess: false})
        } else{
           
            res.send({ userCount : userCount})
        }
}

//Update User
const updateUser = async (req,res) => {


    const user = await User.findByIdAndUpdate(req.params.id,
        {
            name: req.body.name,
            email:req.body.email,
            passwordHash: bcrypt.hashSync(req.body.password, 10),
            isAdmin: req.body.isAdmin,
            phone: req.body.phone,
            apartment: req.body.apartment,
            zip: req.body.zip,
            city: req.body.city,
            country: req.body.country,
            street: req.body.street
    }, 
    {new: true}
    )

    if(!user){
        return res.status(500).send('User is not update')
        } else{
            res.send(user)
        }
}

//Update User Admin
//Priviate
const updateUserAdmin = async (req,res) => {


    const user = await User.findByIdAndUpdate(req.params.id,
        {
            name: req.body.name,
            email:req.body.email,
            passwordHash: bcrypt.hashSync(req.body.password, 10),
            phone: req.body.phone,
            isModerator: req.body.isModerator,
            isAdmin: req.body.isAdmin,
            apartment: req.body.apartment,
            zip: req.body.zip,
            city: req.body.city,
            country: req.body.country,
            street: req.body.street
    }, 
    {new: true}
    )

    if(!user){
        return res.status(500).send('User is not update')
        } else{
            res.send(user)
        }
}

export {addUser,getAllUser,getUserById,loginUser,deleteUser,updateUser,updateUserAdmin,getAllUsersCount}