import Category from "../models/categoryModel.js";
import Product from "../models/productModel.js"; 
import multer from "multer"


const FILE_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpeg',
    'image/jpg': 'jpg',
};

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const isValid = FILE_TYPE_MAP[file.mimetype];
        let uploadError = new Error('invalid image type');

        if (isValid) {
            uploadError = null;
        }
        cb(uploadError, 'public/uploads');
    },
    filename: function (req, file, cb) {
        const fileName = file.originalname.split(' ').join('-');
        const extension = FILE_TYPE_MAP[file.mimetype];
        cb(null, `${fileName}-${Date.now()}.${extension}`);
    },
});

const upload = multer({ storage: storage })

//Create new Product
const addProduct = async (req,res) => {
    const category = await Category.findById(req.body.category)
    console.log("Categorie", category)
    if(!category ){
        return res.status(400).send('Invalid Category')
    }
    
    console.log("Cat",category)
    const file = req.file;
    if (!file) return res.status(400).send('No image in the request');

    const fileName = file.filename;
    const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`
    
    let product = new Product({
        name: req.body.name,
        description: req.body.description,
        richDescription: req.body.richDescription,
        // image: req.file.image,
        // image: filename,
        // image: req.body.image,
        image: `${basePath}${fileName}`,
        brand: req.body.brand,
        price: req.body.price,
        category: req.body.category,
        countInStock: req.body.countInStock,
        rating: req.body.rating,
        numReviews: req.body.numReviews,
        isFeatured: req.body.isFeatured,
    })

    console.log('Create save', product)

   

    product = await product.save();
    
    
    
    
    if(!product){
         res.status(500).send('Product is not created')
    }

        res.send(product)

    
}

//Get all Product or filter with query
const getAllProducts = async (req,res) => {
    
    let filter = {}
    if(req.query.categories){
        filter = {categogry:req.query.categories.split(',')}
    }


    const productList = await Product.find(filter)

    if(!productList){
        return res.status(500).json({sucess: false})
        } else{
            res.send(productList)
        }
}
//Get all Product and select 
// you can filter Product for find your element 
const getAllProductsSelect = async (req,res) => {
    const productList = await Product.find().select('')

    if(!productList){
        return res.status(500).json({sucess: false})
        } else{
            res.send(productList)
        }
}

//Get all Product and populate
// This function get all Product Populate in database 
const getAllProductsPopulate = async (req,res) => {
    const productList = await Product.find().populate('category')

    if(!productList){
        return res.status(500).json({sucess: false})
        } else{
            res.send(productList)
        }
}

//Get Product by Id 
const getProduct = async (req,res) => {
    const productList = await Product.findById(req.params.id)

    if(!productList){
        return res.status(500).json({sucess: false})
        } else{
            res.send(productList)
        }
}

//UpdateProduct
const updateProduct = async (req,res) => {

    const category = await Category.findById(req.body.category)
    if(!category ){
        return res.status(400).send('Invalid Category')
    }
    const product = await Product.findById(req.params.id);
    if (!product) return res.status(400).send('Invalid Product!');


    const file = req.file;
    let imagepath;

    if (file) {
        const fileName = file.filename;
        const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;
        imagepath = `${basePath}${fileName}`;
    } else {
        imagepath = product.image;
    }
    

    const productUpdate = await Product.findByIdAndUpdate(req.params.id,
        {
            name: req.body.name,
            description: req.body.description,
            richDescription: req.body.richDescription,
            image: imagepath,
            brand: req.body.brand,
            price: req.body.price,
            category: req.body.category,
            countInStock: req.body.countInStock,
            rating: req.body.rating,
            numReviews: req.body.numReviews,
            isFeatured: req.body.isFeatured,
    }, 
    {new: true}
    )

    if(!productUpdate){
        return res.status(500).send('Product is not update')
        } else{
            res.send(productUpdate)
        }
}

// delete one Product by Id
const deleteProduct = async(req,res) => {
    Product.findByIdAndRemove(req.params.id).then(product => {
        if(product){
            return res.status(200).json({success: true, message: "Product is remove"})
        } else {
            return res.status(401).json({success: false, message: "Product is not remove"})
        }
    }).catch(err =>{
        return res.status(400).json({success: false, error: err})
    })
}

//Get all Product Count
const getAllProductsCount = async (req,res) => {
    const productCount = await Product.countDocuments()


    if(!productCount){
        return res.status(500).json({sucess: false})
        } else{
           
            res.send({ productCount : productCount})
        }
}

//Get all Product Feature 
const getProductsFeatured = async (req,res) => {
    const product = await Product.find({isFeatured:true})


    if(!product){
        return res.status(500).json({sucess: false})
        } else{
           
            res.send(product)
        }
}

//Get all Product Feature and count 
const getProductsFeaturedCount = async (req,res) => {
    const count = req.params.count ? req.params.count : 0 
    const product = await Product.find({isFeatured:true}).limit(+count)


    if(!product){
        return res.status(500).json({sucess: false})
        } else{
           
            res.send(product)
        }
}
// Gallery Product 

const galleryImageProduct = async (req,res) => {

    const files = req.files;
    let imagesPaths = [];
    const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;

    if (files) {
        files.map((file) => {
            imagesPaths.push(`${basePath}${file.filename}`);
        });
    }

    const product = await Product.findByIdAndUpdate(
        req.params.id,
        {
            images: imagesPaths,
        },
        { new: true }
    );

    if (!product)
        return res.status(500).send('the gallery cannot be updated!');

    res.send(product);
}



export{
    addProduct,getAllProducts,getProduct,getAllProductsSelect,getAllProductsPopulate,updateProduct,deleteProduct,getAllProductsCount,getProductsFeatured,getProductsFeaturedCount,upload,galleryImageProduct
}