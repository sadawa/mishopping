import Order from "../models/orderModel.js";
import OrderItem from "../models/order-item.js";

const getOrder = async (req,res) => {
    const orderList = await Order.find()

    if(!orderList) {
        res.status(500).json({success:false})
    } else {
        res.send(orderList)
    }
}

//get order details
const getOrderPopulateId = async (req,res) => {
    const order = await Order.findById(req.params.id).populate("user",'name')
    .populate({path: 'orderItems',populate:'product'})
    if(!order) {
        res.status(500).json({success:false})
    } else {
        res.send(order)
    }
}

//get order details
const getAllOrdersPopulate = async (req,res) => {
    const orderList = await Order.find().populate('user', 'name').sort({'dateOrdered': -1})

   if(!orderList) {
       res.status(500).json({success:false})
   } else {
       res.send(orderList)
   }
}

// add Order
const addOrder = async (req,res) => {
    const orderItemsIds = Promise.all(req.body.orderItems.map( async orderItem => {
        let newOrderItem = new OrderItem({
            quantity: orderItem.quantity,
            product: orderItem.product
        })

        newOrderItem = await newOrderItem.save()

        return newOrderItem._id
    }))

    const orderItemsIdsResolved = await orderItemsIds

    const totalPrices = await Promise.all(orderItemsIdsResolved.map( async orderItemId =>{
        const orderItem = await OrderItem.findById(orderItemId).populate('product','price')
        const totalPrice = orderItem.product.price*orderItem.quantity
        return totalPrice 
    }))

    

    const totalPrice = totalPrices.reduce((a,b) => a + b , 0)

    let order = new Order({
        orderItems: orderItemsIdsResolved,
        shippingAddress1: req.body.shippingAddress1,
        shippingAddress2: req.body.shippingAddress2,
        city: req.body.city,
        zip: req.body.zip,
        country: req.body.country,
        phone: req.body.phone,
        status: req.body.status,
        totalPrice: totalPrice,
        user: req.body.user,
        
    })
    order = await order.save()

    if(!order){
    return res.status(404).send('Order is not created')
    } else{
        res.send(order)
    }
}

//Update Order by id 
const updateOrderById = async(req,res) => {
    const order = await Order.findByIdAndUpdate(req.params.id,
        {
      status: req.body.status
    }, 
    {new: true}
    )

    if(!order){
        return res.status(404).send('Order is not update')
        } else{
            res.send(order)
        }
}

// delete one Order by Id
const deleteOrder = async(req,res) => {
    Order.findByIdAndRemove(req.params.id).then( async order => {
        if(order){
           await orderItems.map(async orderItem => {
                OrderItem.findByIdAndRemove(orderItem)
            })
            return res.status(200).json({success: true, message: "Order is remove"})
        } else {
            return res.status(401).json({success: false, message: "Order is not remove"})
        }
    })
    
    .catch(err =>{
        return res.status(400).json({success: false, error: err})
    })
}

const totalSales = async (req,res) => {
    const totalSales = await Order.aggregate([
        {$group: {_id:null,totalsales: {$sum: '$totalPrice'}}}
    ])
    if(!totalSales){
        return res.status(400).send('The order sales cannot be generated')
    } else {
        res.send({totalsales: totalSales.pop().totalsales})
    }
}
//Get all Orders Count
const getAllOrdersCount = async (req,res) => {
    const orderCount = await Order.countDocuments()


    if(!orderCount){
        return res.status(500).json({sucess: false})
        } else{
           
            res.send({ orderCount : orderCount})
        }
}

//get order details
const getUserOrders = async (req,res) => {
    const userOrderList = await Order.find({user: req.params.userid}).populate({path: 'orderItems',populate:'product'}).sort({'dateOrdered': -1})

   if(!userOrderList) {
       res.status(500).json({success:false})
   } else {
       res.send(userOrderList)
   }
}


export{
    getOrder,
    addOrder,
    getAllOrdersPopulate,
    getOrderPopulateId,
    updateOrderById,
    deleteOrder,
    totalSales,
    getAllOrdersCount,
    getUserOrders

}