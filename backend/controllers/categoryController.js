import Category from "../models/categoryModel.js";
import User from "../models/userModels.js";

// add Category 
const addCategory = async(req,res) => {
    let category = new Category({
        name: req.body.name,
        icon:req.body.icon,
        color: req.body.color
    })
    category = await category.save()

    if(!category){
    return res.status(404).send('Category is not created')
    } else{
        res.send(category)
    }
}

// delete one Category by Id
const deleteCategory = async(req,res) => {
    Category.findByIdAndRemove(req.params.id).then(category => {
        if(category){
            return res.status(200).json({success: true, message: "Category is remove"})
        } else {
            return res.status(401).json({success: false, message: "Category is not remove"})
        }
    }).catch(err =>{
        return res.status(400).json({success: false, error: err})
    })
}

// Get all Category
const getAllCategory = async (req,res) => {
    const categoryList = await Category.find();

    if(!categoryList) {
        res.status(500).json({success: false})
    } else{
        res.status(200).send(categoryList)
    }
}

// Get one Category by Id 
const getCategoryById = async(req,res) => {
    const category = await Category.findById(req.params.id)
    if(!category){
        return res.status(404).send('Category is not selected')
        } else{
            res.send(category)
        }
}
//Update Category by id 
const updateCategoryById = async(req,res) => {
    const category = await Category.findByIdAndUpdate(req.params.id,
        {
        name: req.body.name,
        icon: req.body.icon,
        color:req.body.color
    }, 
    {new: true}
    )

    if(!category){
        return res.status(404).send('Category is not update')
        } else{
            res.send(category)
        }
}




export {addCategory,deleteCategory,getAllCategory,getCategoryById,updateCategoryById}