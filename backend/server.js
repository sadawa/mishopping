import express from "express"
import dotenv from "dotenv"
import colors from "colors"
import connectDB from "./config/db.js"
import authJwt from "./middleware/jwt.js"
import path from "path"
import errorHandler from "./middleware/error-handler.js"
import bodyParser from "body-parser"
import cors from "cors"
import https from "https"
import morgan from "morgan"



//Import Routes 
import categoryRoutes from "./routers/categoryRoutes.js"
import productRoutes from "./routers/productRoutes.js"
import userRoutes from './routers/userRoutes.js'
import orderRoutes from './routers/orderRoutes.js'

dotenv.config()

connectDB()


//Middleware
const app = express()
app.use(express.json())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended : true}));
app.use(cors());
app.options('*', cors())

const __dirname = path.resolve()
app.use('/public/uploads', express.static(path.join(__dirname, '/public/uploads')))

// app.use('/public/uploads', express.static(__dirname , '/public/uploads'));
// app.use(express.static(path.join(__dirname,  '/public/uploads')))



if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'))
  }
  app.use(authJwt())
  app.use(errorHandler)

 



  app.use('/api/categories',categoryRoutes)
  app.use('/api/produits',productRoutes)
  app.use('/api/users',userRoutes)
  app.use('/api/orders',orderRoutes)





const PORT = process.env.PORT || 5000

app.listen(PORT, console.log(`Server runnig in ${PORT} mode on port ${PORT}`.yellow.bold))